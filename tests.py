from matrix_spiral_order import get_spiral_order

INPUT_MATRIX_1 = [
    ["1", "2", "3", "4"],
    ["5", "6", "7", "8"],
    ["9", "A", "B", "C"],
    ["D", "E", "F", "G"],
]

OUTPUT_1 = [
    "1",
    "2",
    "3",
    "4",
    "8",
    "C",
    "G",
    "F",
    "E",
    "D",
    "9",
    "5",
    "6",
    "7",
    "B",
    "A",
]


INPUT_MATRIX_2 = [
    ["1", "2", "3"],
    ["5", "6", "7"],
    ["9", "A", "B"],
    ["D", "E", "F"],
]

OUTPUT_2 = [
    "1",
    "2",
    "3",
    "7",
    "B",
    "F",
    "E",
    "D",
    "9",
    "5",
    "6",
    "A",
]


INPUT_MATRIX_3 = [["1", "2", "3", "4"], ["6", "7", "8", "9"]]

OUTPUT_3 = [
    "1",
    "2",
    "3",
    "4",
    "9",
    "8",
    "7",
    "6",
]


INPUT_MATRIX_4 = [["1", "2", "3", "4"]]

OUTPUT_4 = [
    "1",
    "2",
    "3",
    "4",
]


INPUT_MATRIX_5 = [
    ["1"],
    ["2"],
    ["3"],
    ["4"],
]

OUTPUT_5 = [
    "1",
    "2",
    "3",
    "4",
]

def main():
    # TEST 1
    actual_output_1 = get_spiral_order(INPUT_MATRIX_1)
    test_1_result = actual_output_1 == OUTPUT_1
    print(f"Test 1: {test_1_result}")

    # TEST 2
    actual_output_2 = get_spiral_order(INPUT_MATRIX_2)
    test_2_result = actual_output_2 == OUTPUT_2
    print(f"Test 2: {test_2_result}")

    # TEST 3
    actual_output_3 = get_spiral_order(INPUT_MATRIX_3)
    test_3_result = actual_output_3 == OUTPUT_3
    print(f"Test 3: {test_3_result}")

    # TEST 4
    actual_output_4 = get_spiral_order(INPUT_MATRIX_4)
    test_4_result = actual_output_4 == OUTPUT_4
    print(f"Test 4: {test_4_result}")

    # TEST 5
    actual_output_5 = get_spiral_order(INPUT_MATRIX_5)
    test_5_result = actual_output_5 == OUTPUT_5
    print(f"Test 5: {test_5_result}")


if __name__ == "__main__":
    main()

