def get_spiral_order(matrix):
    result = []

    rows_count = len(matrix)
    if rows_count == 0:
        return result

    columns_count = len(matrix[0])
    if columns_count == 0:
        return result

    directionX = [1, 0, -1, 0]
    directionY = [0, 1, 0, -1]
    old_cells = []

    x_index = 0
    y_index = 0
    cell_index = (y_index, x_index)
    direction_index = 0

    while cell_index not in old_cells:
        element = matrix[y_index][x_index]
        result.append(element)
        old_cells.append((y_index, x_index))

        new_x_index = x_index + directionX[direction_index]
        new_y_index = y_index + directionY[direction_index]

        if (
            new_x_index >= columns_count
            or new_x_index < 0
            or new_y_index >= rows_count
            or new_y_index < 0
            or (new_y_index, new_x_index) in old_cells
        ):
            new_direction_index = direction_index + 1
            direction_index = new_direction_index % 4

            new_x_index = x_index + directionX[direction_index]
            new_y_index = y_index + directionY[direction_index]

        if (
            new_x_index >= columns_count
            or new_x_index < 0
            or new_y_index >= rows_count
            or new_y_index < 0
            or (new_y_index, new_x_index) in old_cells
        ):
            break

        x_index = new_x_index
        y_index = new_y_index
        cell_index = (y_index, x_index)

    return result
